const API_ROOT = window.location.port == 3000 ? "http://localhost:8080" : ""

const callApi = (config = {}) => {

  const fullUrl = `${API_ROOT}${config.url}`

  var responseType = "json"

  if (config && config.responseType) {

    responseType = config.responseType;

  }

  return fetch(fullUrl, config)

    .then(response => response[responseType]()

      .then(json => {

        if (!response.ok) {
          return Promise.reject(json)
        }

        return Object.assign({}, json)
      }))
}

export default store => next => action => {

  switch (action.type) {

    case "HTTP_REQUEST": {
      return callApi(action.config)

        .then((response) => {

          return next({
            type: `${action.requestName}_SUCCESS`
          })

        })
    }
    default:
      return next(action)
  }

}