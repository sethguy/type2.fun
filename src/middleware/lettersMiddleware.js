import { generateNewLetterData, initLoadedFonts } from '../Services/letterDataService'

import { typesWaitService } from '../Components/typesWaitScreen/service'

export default store => next => action => {

  switch (action.type) {

    case "initFontService": {

      next({
        type: "STATE_UPDATE",
        update: {
          fontsLoaded: false
        }
      })

      var {letters} = store.getState();

      var neededFonts = letters

        .map((letterData) => {

          return letterData.style.fontFamily;

        })

      return initLoadedFonts(neededFonts)

        .then((loadedFonts) => {

          typesWaitService.hide()

          next({
            type: "fontsLoaded",
            fonts: loadedFonts
          })

        })

    }
    case "updateLetterChars": {

      var {loadedFonts} = store.getState()

      var letters = []

      while (letters.length < action.word.length) {

        var oldLetterData = store.getState().letters[letters.length];

        var newLetterData = generateNewLetterData(loadedFonts);

        var letterData = oldLetterData ? oldLetterData.style : newLetterData;

        letters.push({
          style: letterData,
          char: action.word[letters.length]
        })

      }

      return next({
        type: "STATE_UPDATE",
        update: {
          letters,
          mainInputValue: action.word
        }
      })

    }
    default:
      return next(action)
  }

}