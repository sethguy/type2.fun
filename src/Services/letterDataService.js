import { Observable } from 'rxjs'

import WebFont from 'webfontloader';

var googleFontsUrl = "https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyA3Oq8AzMQo3nZzOeKMcUatlmZ_Q08NPV0";

export const generateNewLetterData = function(loadedFonts) {

  return {
    ...getStyle(),
    color: getRandomColorHex(),
    fontFamily: getRandomFontName(loadedFonts)
  }

}

var getStyle = function() {

  return {

    fontSize: '10vh',

  }

}

export async function initLoadedFonts(neededFonts) {

  var fontJson = await getFontJson();

  var selectedFonts = await selectFontsFromList(20, fontJson)

  var missingFonts = getMissingFonts(neededFonts, selectedFonts);

  await loadRandomFonts([...selectedFonts, ...missingFonts]);

  return selectedFonts

}

var getMissingFonts = function(neededFonts, selectedFonts) {

  return neededFonts

    .filter(needed => {

      return !selectedFonts
        .map(font => font.family)
        .includes(needed)

    })

    .map((family) => {

      return {
        family
      }

    })

}

var selectFontsFromList = function(num, wholeFontList) {
  return new Promise(function(resolve, reject) {

    Observable.range(0, num)

      .map((count) => {

        return getRandomFont(wholeFontList.items)
      })

      .scan((goodfonts, nextFont) => {

        goodfonts.push(nextFont)

        return goodfonts;

      }, [])

      .filter((goodfonts => goodfonts.length == num))

      .subscribe((list) => resolve(list))

  });
}

var formatHexlet = function(part) {

  if (part.length < 2)
    return part = "0" + part

  return part;
}

var getRandomHexlet = () => {

  return formatHexlet(Math.floor(Math.random() * 255).toString(16))

}

var getRandomColorHex = function() {

  return `#${getRandomHexlet()}${getRandomHexlet()}${getRandomHexlet()}`;

}

var getRandomFontName = function(fontList) {

  return fontList[Math.floor(Math.random() * fontList.length)].family;

}

var loadRandomFonts = function(pickRandomFonts) {

  return new Promise(function(resolve, reject) {

    var newfams = pickRandomFonts.map(font => font.family)

    WebFont.load({
      google: {
        families: newfams
      },

      active: () => {
        resolve()
      }

    });

  });

}

var getFontJson = function() {

  return fetch(googleFontsUrl)

    .then(getFontListResponse => getFontListResponse.json())

}

var getRandomFont = function(list) {

  var size = list.length;

  return list[Math.floor(Math.random() * size - 1)]
}