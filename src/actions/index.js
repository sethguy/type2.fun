import {lettersConfig} from '../interactConfigs/letters'

export const initFontService = (data) => (dispatch, getState) => {

  return dispatch({
    type: "initFontService"
  })

}



export const initLetterInterAct = (data) => (dispatch, getState) => {

  lettersConfig({dispatch, getState})

  return dispatch({
    type: "initLetterInterAct"
  })

}