import React, { Component } from 'react';
import { render } from 'react-dom';
import { Provider, connect } from 'react-redux'

import { store } from './Store';

import { HashRouter } from 'react-router-dom'

import createHistory from 'history/createBrowserHistory'

import Home from './Components/Home/';

import TypesWaitScreen from './Components/typesWaitScreen/';

import PaymentPage from './Components/PaymentPage/';

import { StripeProvider } from 'react-stripe-elements';
import { Route } from 'react-router-dom'

const Root = ({store, history}) => (
  <Provider store={ store }>
    <StripeProvider apiKey="pk_test_rPSGAU5ciy3i5pHlgjUtINnz">
      <HashRouter history={ history } basename='/'>
        <div className="h-100">
          <Route exact path="/" component={ Home } />
          <Route exact path="/payment-page" component={ PaymentPage } />
          <TypesWaitScreen/>
        </div>
      </HashRouter>
    </StripeProvider>
  </Provider>
)

render(<Root store={ store } />, document.getElementById('root'));