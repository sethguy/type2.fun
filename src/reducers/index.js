import { combineReducers } from 'redux'

const user = (state = {}, action) => {
  switch (action.type) {

    default:
      return state;
  }

}

export const rootReducer = (state = {}, action) => {
  switch (action.type) {

    case 'fontsLoaded': {

      return {
        ...state,
        loadedFonts: action.fonts,
        fontsLoaded: true,
      }

    }
    case 'STATE_UPDATE': {

      return {
        ...state,
        ...action.update
      }

    }
    default:
      return state;
  }

}