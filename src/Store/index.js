import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'

import { rootReducer } from '../reducers/'

import { httpRequest, lettersMiddleware } from '../middleware';

const configureStore = preloadedState => {
  const store = createStore(
    rootReducer,
    preloadedState,
    compose(
      applyMiddleware(thunk, httpRequest, lettersMiddleware, createLogger()),
    )
  )

  return store
}

var preloadedState = {
  user: {
    name: "seth"
  },
  letters: []
}

var types2funStateKey = 'types.fun.preloadedState';

var StoredState = localStorage.getItem(types2funStateKey) || JSON.stringify(preloadedState);

preloadedState = JSON.parse(StoredState)

console.log(' preloadedState ', preloadedState)

preloadedState = {
  ...preloadedState,
  loadedFonts: []
}

export const store = configureStore(preloadedState);

store.subscribe(() => {

  var state = store.getState();

  localStorage.setItem(types2funStateKey, JSON.stringify(state))
})