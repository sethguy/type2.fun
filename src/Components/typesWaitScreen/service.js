import { Observable, Subject, ReplaySubject } from 'rxjs'

var typesWaitScreenSubject = new ReplaySubject();

var typesWaitService = {

  subject: typesWaitScreenSubject,

  show: () => {
    typesWaitScreenSubject.next({
      zplace: 9999,
      on: true
    })
  },

  hide: () => {
    typesWaitScreenSubject.next({
      zplace: -1,
      on: false
    })
  }

}

export { typesWaitService }