import React, { Component } from 'react';
import { Observable, Subject } from 'rxjs'
import './typesWaitScreen.css'

import { typesWaitService } from './service'


export default class TypesWaitScreen extends Component {

  constructor(props) {
    super(props);

    this.state = {
      zplace: 9999,
      flies: [],
      on: true

    };

    Observable.interval(50)

      .map((num) => this.random())

      .subscribe((rando) => {

        this.addAFly(rando)

        if (this.state.flies.length > 80) {

          this.removeAFly()

        }

      })

  }

  addAFly(fly) {
    var stuff = this.state.flies;

    stuff.push(fly)

    this.setState({
      flies: stuff
    })
  }

  removeAFly() {

    this.state.flies.splice(0, 1)

    this.setState({
      flies: this.state.flies
    })

  }

  random() {

    var randomTop = Math.random() * 100 + "%";

    var randomLeft = Math.random() * 100 + "%";

    var what = "typesdfunz"

    return (
      <img key={ (new Date()).getTime() + Math.random() } className="waitLet" style={ { top: randomTop, left: randomLeft } } src={ this.sorceType(what) }></img>
    )

  }

  sorceType(what) {
    var index = Math.floor(Math.random() * what.length)


    var imgName = what[index]
    if (imgName == 'z') return `/imgs/letters/${imgName}.jpg`
    return `/imgs/letters/${imgName}.png`
  }

  componentDidMount() {
    typesWaitService.subject

      .subscribe((stream) => {

        console.log("setem", stream, this)

        this.setState({
          zplace: stream.zplace,
          on: stream.on,
          flies: []

        })

      })
  }

  render() {

    return (
      <div style={ { zIndex: this.state.zplace } } className="typesWaitScreen">
        <div className="typesWaitfade"></div>
        <div className="typesWaitShow">
          { this.state.on ?
            this.state.flies : "" }
        </div>
      </div>

      );
  }
}

export { TypesWaitScreen }