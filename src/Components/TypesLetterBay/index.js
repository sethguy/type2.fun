import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import {initLetterInterAct} from '../../actions'
import Letters from './Letters';
import interact from 'interactjs';

import './typesLetterBay.css'

class TypesLetterBayComponet extends Component {
  constructor() {
    super();
  }
  componentDidMount(){
    interact('.letters').unset()

    this.props.initLetterInterAct()
  }

  render() {
    const {letters} = this.props;

    return (
      <div className ="letters" id="types-letter-bay">
        <Letters {...{ letters }}/>
      </div>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  letters: state.letters,
})

const TypesLetterBay = connect(mapStateToProps, {
  initLetterInterAct
})(TypesLetterBayComponet)

export default TypesLetterBay