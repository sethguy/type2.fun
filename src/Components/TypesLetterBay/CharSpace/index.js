import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { v4 } from 'uuid'

class CharSpaceComponet extends Component {
  constructor() {
    super();

  }

  render() {

    var {letter} = this.props;

    return (<span className="types-char-space" style={ letter.style } key={ v4() }>{ letter.char }</span>);
  }
}

const mapStateToProps = (state, ownProps) => ({
})

const CharSpace = connect(mapStateToProps, {

})(CharSpaceComponet)

export default CharSpace