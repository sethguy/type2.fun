import React, { Component } from 'react';

import { v4 } from 'uuid'
import CharSpace from './CharSpace/';

export default ({letters}) => letters
  .map((letter) => (
    <CharSpace key={ v4() } {...{ letter }} />)
)

