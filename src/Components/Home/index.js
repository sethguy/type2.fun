import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import Topbar from '../Topbar/';

import TypesMainInput from '../TypesMainInput/';
import TypesMainStage from '../TypesMainStage/';

import { initFontService } from '../../actions'

class HomeComponet extends Component {
  constructor() {
    super();

  }

  componentDidMount() {

    this.props.initFontService()

  }

  render() {

    return (
      <div className="h-100 d-flex flex-column">
        <Topbar/>
        <TypesMainStage/>
        <TypesMainInput/>
      </div>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
})

const Home = connect(mapStateToProps, {
  initFontService
})(HomeComponet)

export default Home