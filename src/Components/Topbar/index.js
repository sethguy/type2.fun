import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'
import { Route } from 'react-router-dom'
import { v4 } from 'uuid'

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';


class TopbarComponet extends Component {
  constructor() {
    super();

  }

  render() {
    return (

      <AppBar position="static">
        <Toolbar className="d-flex justify-content-between">
          <Typography variant="title" color="inherit">
            types.fun
          </Typography>
          <IconButton color="inherit" aria-label="Menu">
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
})

const Topbar = connect(mapStateToProps, {

})(TopbarComponet)

export default Topbar