import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { v4 } from 'uuid'

import './mainInput.css'
import { updateLetterChars } from './actions'

class TypesMainInputComponet extends Component {
  constructor() {
    super();

  }

  onInput = (event, value) => {

    this.props.updateLetterChars({
      word: value
    })

  }

  render() {
    const {letters, mainInputValue} = this.props;

    return (
      <div style={ { backgroundColor: 'black' } }>
        <input placeholder="type something fun !" onChange={ (event) => {
                                                               this.onInput(event, event.target.value)
                                                             } } value={ mainInputValue } className="types-input w-100" />
      </div>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  letters: state.letters,
  mainInputValue: state.mainInputValue
})

const TypesMainInput = connect(mapStateToProps, {
  updateLetterChars
})(TypesMainInputComponet)

export default TypesMainInput