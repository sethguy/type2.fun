export const updateLetterChars = ({word}) => (dispatch, getState) => {

  return dispatch({
    type: "updateLetterChars",
    word
  })
}
