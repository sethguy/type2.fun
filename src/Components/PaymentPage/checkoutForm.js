import React, { Component } from 'react';
import { CardElement, injectStripe } from 'react-stripe-elements';
import { getQueryParams, getPathVariables } from '../../Services/utils'

import { Observable } from 'rxjs'

class CheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  submit = (ev) => {

  }

  createToken = () => Observable.fromPromise(this.props.stripe.createToken({
    name: `payment_token`
  }))

  processPayment = (token, _id) => {

  }

  render() {
    return (
      <div style={ { minWidth: 300 } } className="checkout d-flex flex-column  justify-content-center">
        <CardElement />
        <br />
        <br />
        <button className="btn btn-info brandong" onClick={ this.submit }>
          Submit Payment
        </button>
      </div>
      );
  }
}

export default injectStripe(CheckoutForm);