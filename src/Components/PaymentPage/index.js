import React, { Component } from 'react';

import { Elements } from 'react-stripe-elements';
import CheckoutForm from './checkoutForm'
import { getQueryParams, getPathVariables } from '../../Services/utils'


class PaymentPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    var {chargeType} = getQueryParams()

    return (
      <div className="h-100 d-flex flex-column justify-content-center align-items-center">
        <Elements>
          <CheckoutForm />
        </Elements>
      </div>
      );
  }
}

export default PaymentPage;