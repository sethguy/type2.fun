import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'
import TypesLetterBay from '../TypesLetterBay/';

import { v4 } from 'uuid'

class TypesMainStageComponet extends Component {
  constructor() {
    super();
  }

  render() {
    const {letters, fontsLoaded} = this.props;

    return (

      <div className="flex-1 d-flex flex-wrap justify-content-center text-center scroll">
        { fontsLoaded && <TypesLetterBay/> }
      </div>

      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  fontsLoaded: state.fontsLoaded
})

const TypesMainStage = connect(mapStateToProps, {
})(TypesMainStageComponet)

export default TypesMainStage