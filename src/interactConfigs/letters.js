
import interact from 'interactjs';


const dragMoveListener = (event) => {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
}



const lettersConfig = (props) => {
    const { dispatch, getState } = props;

    interact('.letters').draggable({
        onmove: dragMoveListener,
        modifiers: [
            interact.modifiers.restrict({
                restriction: 'parent',
                elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
            })
        ],
        restriction: 'parent',
        restrict: {
            endOnly: true,
            elementRect: {
                top: 0,
                left: 0,
                bottom: 1,
                right: 1
            }
        },
    })
}

export { lettersConfig }